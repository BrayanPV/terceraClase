document.querySelector('#miboton').addEventListener('click', traer);

function traer() {
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'https://www.datos.gov.co/resource/ax3m-2qmb.json');
    xhttp.send();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
            let datos = JSON.parse(this.responseText);
            console.log(datos);
            let res = document.querySelector('#res');
            res.innerHTML = '';
            for (let item of datos) {
                res.innerHTML += `<tr>
                    <td>${item.municipio}</td>
                    <td>${item.direcci_n}</td>
                    <td>${item.estado}</td>`;
            }
        } else {
            console.log("cargando...");
        }
    }
}
